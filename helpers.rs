/* BEGIN: LinkedList */
struct LinkedList<T> {
    head: Cons<T>
}

struct Cons<T> {
    value: T,
    next: Option<Cons<T>>
}

impl LinkedList {
    fn new<T>(init_val: &T) -> Self {
        LinkedList<T> {
            head: Cons<T> {
                value: init_val,
                next: None
            }
        }
    }

    fn prepend(&mut self, cell: Cons) {
        cell.next = self.head
        self.head = cell
    }

    fn append(&mut self, cell: Cons) {
        let mut current = self.head
        loop {
            match current.next {
                Some(x) => {},
                None => break
            }
            current = current.next
        }
        current.next = cell
    }
}
/* END: LinkedList */