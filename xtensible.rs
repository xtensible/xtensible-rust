/* xtensible
 * A plugin metaframework
 */

mod xtensible {
    mod helpers;
    mod perform;
    mod traits;
}