// Main traits used in xtensible.
use super::helpers;

// Allows for validation and automated processes
// to fix a request if it does not pass validation.
trait BreakingChanges {
    // Checks for breaking changes.
    fn isBreaking(&self, other: &Self) -> bool;
    // If there are, this function reports if they can be fixed.
    fn fixable(&self, other: &Self) -> bool;
    // This function tries to fix breaking changes.
    fn tryPatch(&self, other: &mut Self);
}

// Ensures that an object can be
// performed. Mainly for xtensible::types::Command.
trait ImplementsPerf {
    let mut hooks: LinkedList<PerfHook>;

    // Performs action. This function MUST
    // run hooks with data to be used, if any.
    fn perf(&self);
    // Should automatically clone internal hook.
    fn hook(&self) -> LinkedList<PerfHook>;
    // If this function returns false, it is safe to facade
    // all hook-related functions and variables.
    fn allowsPerfHook(&self) -> bool;
    // Allows you to modify the default PerfHook and apply changes.
    // Uses PerfHook::isBreaking.
    fn rehook(&self, &LinkedList<PerfHook>);
}